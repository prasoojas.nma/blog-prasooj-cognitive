<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'James',
            'second_name' => 'Smith',
            'user_name' => 'james',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('12345678'),
            'is_admin' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('users')->insert([
            'name' => 'Prasooj',
            'second_name' => 'AS',
            'user_name' => 'prasooj',
            'email' => 'prasooj@gmail.com',
            'password' => Hash::make('12345678'),
            'is_admin' => 0,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('users')->insert([
            'name' => 'Arjun',
            'second_name' => 'KV',
            'user_name' => 'prasooj',
            'email' => 'user@gmail.com',
            'password' => Hash::make('12345678'),
            'is_admin' => 0,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
