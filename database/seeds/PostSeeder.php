<?php

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'user_id' => 3,
            'title' => 'Tutorials and How-to Guides',
            'description' => 'Tutorials and How-to guides are probably the simplest type of blog post you can work on. They are easy because they involve you talking about things you are already familiar with such as your product or service.',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('posts')->insert([
            'user_id' => 2,
            'title' => 'Controversial Subjects',
            'description' => 'This type of blog post can be really fun to write, but remember to be wise. Controversy is always newsworthy — just look at the local news, they’re always reporting on different controversies!',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('posts')->insert([
            'user_id' => 3,
            'title' => 'Latest Industry Newss',
            'description' => 'You have your morning routine, just like I do. You skim the online news and find the latest trends in your industry. Why not make your morning routine into a blog post',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
