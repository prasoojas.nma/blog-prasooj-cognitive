@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"> <strong> {{ __('Create New Post') }} </strong>  </div>

                <div class="card-body">

                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                  <form method="post" action="{{ route('posts.store') }}">

                      @csrf

                      <div class="form-group">
                          <label class="label">Title</label>
                          <div class="control">
                              <input class="form-control"  type="text" name="title" value="{{ old('title') }}" class="input" placeholder="Title"  maxlength="100"  />
                          </div>
                      </div>

                      <div class="form-group">
                          <label class="label">Description</label>
                          <div class="control">
                              <textarea class="form-control" name="description" class="textarea" placeholder="Description" maxlength="2000"  rows="5">{{ old('description') }}</textarea>
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="control">
                              <button type="submit" class="btn btn-info">Create</button>
                          </div>
                      </div>

                  </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
