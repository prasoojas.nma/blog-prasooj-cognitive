@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-12">
         @if (session('notification'))
         <div class="alert alert-success">
            {{ session('notification') }}
         </div>
         @endif

         @if ($errors->any())
             <div class="alert alert-danger">
                 <ul>
                     @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                     @endforeach
                 </ul>
             </div>
         @endif
         <div class="card">
            <div class="card-header"> <strong> {{ __('View Post') }} </strong>  </div>
            <div class="card-body">
               <div class="content">
                  <a href="{{ route('posts.show', [$post->id]) }}">
                     <h1 class="title">{{ $post->title }}</h1>
                  </a>
                  <p>{!! nl2br(e($post->description)) !!}</p>
                  <p style="color:gray;"> {{ $post->created_at->diffForHumans() }} by {{ $post->user->name}}</p>
                  @if(Auth::user()->id == $post->user_id)

                     <div class="field is-grouped">
                        <div class="control">
                           <a class="btn btn-sm btn-outline-primary" href="{{ route('posts.edit', [$post->id])}}">Edit</a>
                        </div>
                        <!-- <div class="control">
                           <button type="submit" class="button is-danger is-outlined">
                             Delete
                           </button>
                           </div> -->
                     </div>
                  @endif
               </div>
            </div>
         </div>
         <hr>
         <!-- comment section start -->
         <div class="card">
            <div class="card-header"> <strong> {{ __('Comments') }} </strong>  </div>
            <div class="card-body">
               <div class="content">

                  @if(Auth::user()->id == $post->user_id)
                     @forelse ($post->comments as $comment)
                     {{$comment->description}}<br>
                     <span style="color:gray;">{{ $comment->created_at->diffForHumans() }} by {{ $comment->user->name}}</span>
                     <hr>
                     @empty
                     <p>No Comments</p>
                     @endforelse

                  @endif
               </div>
            </div>
         </div>
         <!-- comment section end -->

         <!--Submit comment section start -->
         <div class="card">
            <div class="card-body">
               <div class="content">



                 <form method="post" action="{{ route('comment.create', [$post->id]) }}">

                     @csrf

                     <div class="form-group">
                         <label class="label">Comment</label>
                         <div class="control">
                             <textarea class="form-control" name="comment" class="textarea" placeholder="Commet" maxlength="2000"  rows="5">{{ old('comment') }}</textarea>
                         </div>
                     </div>

                     <div class="form-group">
                         <div class="control">
                             <button type="submit" class="btn btn-info">Submit</button>
                         </div>
                     </div>

                 </form>
               </div>
            </div>
         </div>
         <!--Submit comment section end -->

      </div>
   </div>
</div>
@endsection
