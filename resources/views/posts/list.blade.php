@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-12">
         @if (session('notification'))
         <div class="alert alert-success">
            {{ session('notification') }}
         </div>
         @endif
         @if (session('error_message'))
         <div class="alert alert-danger">
            {{ session('error_message') }}
         </div>
         @endif
         <div class="card">
            <div class="card-header">
              <strong> {{ __('All Post') }} </strong>
              <span style="float:right"> <a class="btn btn-sm btn-outline-primary" href="{{ route('posts.create') }}"> Add New</a>  </span> </div>
            <div class="card-body">

                @forelse ($posts as $post)
                <div class="content">
                   <a href="{{ route('posts.show', [$post->id]) }}">
                      <h1 class="title">{{ $post->title }}</h1>
                   </a>

                   <p style="color:gray;"> {{ $post->created_at->diffForHumans() }} by {{ $post->user->name}}</p>
                   @if(Auth::user() && Auth::user()->id == $post->user_id)
                   <form method="post" action="{{ route('posts.destroy', [$post->id]) }}">
                      @csrf @method('delete')
                      <div class="field is-grouped">
                         <div class="control">
                            <a class="btn btn-sm btn-outline-primary" href="{{ route('posts.edit', [$post->id])}}">Edit</a>
                         </div>
                         <!-- <div class="control">
                            <button type="submit" class="button is-danger is-outlined">
                              Delete
                            </button>
                            </div> -->
                      </div>
                   </form>

                   @endif
                   <hr>
                </div>
                @empty
                <p>No Posts</p>
                @endforelse


            </div>
         </div>
      </div>
   </div>
</div>
@endsection
