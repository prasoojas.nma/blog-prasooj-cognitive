@extends('admin.layout')
@section('content')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-12">
         @if (session('notification'))
         <div class="alert alert-success">
            {{ session('notification') }}
         </div>
         @endif
         @if (session('error_message'))
         <div class="alert alert-danger">
            {{ session('error_message') }}
         </div>
         @endif
         <div class="card">
            <div class="card-header">
              <strong> {{ __('All Post') }} </strong>
            </div>
            <div class="card-body">



                <table class="" style="width: 100%;border:1px solid #ccc">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th>User</th>
                      <th>Created at</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                      @forelse($posts as $post)
                      <tr>
                        <td>{{$post->title}}</td>
                        <td>{{$post->user->name}}</td>
                        <td>{{$post->created_at}}</td>
                        <td> <a class="btn btn-sm btn-outline-primary" href="{{ route('admin.post.edit', [$post->id])}}">Edit</a> </td>
                      </tr>
                      @empty
                      <p>No Posts</p>
                      @endforelse


                  </tbody>
                </table>


            </div>
         </div>
      </div>
   </div>
</div>
@endsection
