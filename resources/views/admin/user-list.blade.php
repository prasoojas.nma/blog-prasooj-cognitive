@extends('admin.layout')
@section('content')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-12">
         @if (session('notification'))
         <div class="alert alert-success">
            {{ session('notification') }}
         </div>
         @endif
         @if (session('error_message'))
         <div class="alert alert-danger">
            {{ session('error_message') }}
         </div>
         @endif
         <div class="card">
            <div class="card-header">
              <strong> {{ __('User List With post Count') }} </strong>
              <span style="float:right"> <a class="btn btn-sm btn-outline-primary" href="{{ route('admin.dashboard') }}"> Back</a>  </span>
            </div>
            <div class="card-body">



                <table class="" style="width: 100%;border:1px solid #ccc">
                  <thead>
                    <tr>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Post Count</th>
                    </tr>
                  </thead>
                  <tbody>

                      @forelse($users as $user)
                      <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->second_name}}</td>
                        <td>{{$user->posts_count}}</td>
                      </tr>
                      @empty
                      <p>No Posts</p>
                      @endforelse


                  </tbody>
                </table>


            </div>
         </div>
      </div>
   </div>
</div>
@endsection
