@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

          @if (session('notification'))
          <div class="alert alert-success">
             {{ session('notification') }}
          </div>
          @endif
          @if (session('error_message'))
          <div class="alert alert-danger">
             {{ session('error_message') }}
          </div>
          @endif

            <div class="card">
                <div class="card-header">  <strong> {{ __('Edit Post') }} </strong>
                  <span style="float:right"> <a class="btn btn-sm btn-outline-primary" href="{{ route('admin.dashboard') }}"> Back</a>  </span>

                 </div>

                <div class="card-body">

                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif



                  <form method="post" action="{{ route('admin.post.update', [$post->id]) }}">

                  @csrf


                  <div class="form-group">
                      <label class="label">User</label> : {{ $post->user->name }}
                      <span style="color:gray;"> | {{ $post->created_at->diffForHumans() }} </span>
                      <div class="control">

                          <!-- <input class="form-control"  type="text" name="" value="{{ $post->user->name }}" class="input" placeholder=""  maxlength="100" readonly /> -->
                      </div>
                  </div>

                  <div class="form-group">
                      <label class="label">Title</label>
                      <div class="control">
                          <input class="form-control"  type="text" name="title" value="{{ $post->title }}" class="input" placeholder="Title"  maxlength="100"  />
                      </div>
                  </div>

                  <div class="form-group">
                      <label class="label">Description</label>
                      <div class="control">
                          <textarea class="form-control" name="description" class="textarea" placeholder="Description" maxlength="2000"  rows="5">{{ $post->description }}</textarea>
                      </div>
                  </div>


                  <div class="field">
                      <div class="control">
                          <button type="submit" class="btn btn-info">Update</button>
                      </div>
                  </div>

              </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
