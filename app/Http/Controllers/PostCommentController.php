<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\PostComment;
use Auth;

class PostCommentController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function create(Request $request,Post $post)
    {
      // Validate posted form data
      $validated = $request->validate([
          'comment' => 'required|string|min:5|max:100',
      ]);

      $comment = new PostComment;
      $comment->post_id = $post->id;
      $comment->description = $request->comment;
      $comment->user_id = Auth::user()->id;
      $comment->save();

      return redirect()->back()->with('notification', 'Comment Added!');
    }
}
