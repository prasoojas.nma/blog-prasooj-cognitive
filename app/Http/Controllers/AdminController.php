<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\PostComment;
use App\User;
use Auth;

class AdminController extends Controller
{
  public function dashboard()
  {
      $posts = Post::latest()->get();
      return view('admin.dashboard', compact('posts'));
  }

  public function postEdit(Post $post)
  {
      return view('admin.posts_edit', compact('post'));
  }

  public function postUpdate(Request $request, Post $post)
  {
      // Validate posted form data
      $validated = $request->validate([
          'title' => 'required|string|min:5|max:100',
          'description' => 'required|string|min:5|max:2000',
      ]);

      //check the user is the ownwer of the post
      if (Auth::user()->is_admin === 1) {
          // Update Post with validated data
          $post->update($validated);

          // Redirect the user to the created post woth an updated notification
          return redirect()->back()->with('notification', 'Post updated!');
     }

     return redirect()->back()->with('error_message', 'No access Permission!');

  }

  public function userList()
  {
      $users = User::latest()->withCount('posts')->get();
      // return $users;
      return view('admin.user-list', compact('users'));
  }

  public function postList()
  {
      $posts = Post::latest()->withCount('comments')->get();
      // return $posts;
      return view('admin.post-list', compact('posts'));
  }
}
