<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;

class PostController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get all Posts, ordered by the newest first
        $posts = Post::latest()->get();

        // Pass Post Collection to view
        return view('posts.list', compact('posts'));
    }

    public function list()
    {
        // Get all Posts, ordered by the newest first
        $posts = Post::latest()->get();

        // Pass Post Collection to view
        return view('posts.list', compact('posts'));
    }

    public function myPosts()
    {
        // Get all Posts, ordered by the newest first
        $posts = Post::latest()->where('user_id',Auth::user()->id)->get();

        // Pass Post Collection to view
        return view('posts.myPosts', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
         return view('posts.create');
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate posted form data
        $validated = $request->validate([
            'title' => 'required|string|unique:posts|min:5|max:100',
            'description' => 'required|string|min:5|max:2000',
        ]);

        // Create and save post with validated data
        $post = new Post;
        $post->title = $request->title;
        $post->description = $request->description;
        $post->user_id = Auth::user()->id;
        $post->save();

        // Redirect the user to the created post with a success notification
        return redirect(route('posts.show', [$post->id]))->with('notification', 'Post created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit(Post $post)
     {
         return view('posts.edit', compact('post'));
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, Post $post)
     {
         // Validate posted form data
         $validated = $request->validate([
             'title' => 'required|string|min:5|max:100',
             'description' => 'required|string|min:5|max:2000',
         ]);

         //check the user is the ownwer of the post
         if ($post->user_id === Auth::user()->id) {
             // Update Post with validated data
             $post->update($validated);

             // Redirect the user to the created post woth an updated notification
             return redirect()->back()->with('notification', 'Post updated!');
        }

        return redirect()->back()->with('error_message', 'No access Permission!');

     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
