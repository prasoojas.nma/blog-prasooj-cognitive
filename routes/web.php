<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', 'PostController@index');
Route::get('/home', 'HomeController@index')->name('home');

//user routes
Route::group(['middleware' => ['auth']], function () {
  Route::resource('posts', 'PostController');
  Route::get('my-posts', 'PostController@myPosts')->name('my-posts');
  Route::post('comment/create/{post}', 'PostCommentController@create')->name('comment.create');
});

//admin routes
Route::group(['middleware' => ['auth','admin']], function () {
    Route::get('admin-dashboard', 'AdminController@dashboard')->name('admin.dashboard');
    Route::get('admin/post/edit/{post}', 'AdminController@postEdit')->name('admin.post.edit');
    Route::post('admin/post/update/{post}', 'AdminController@postUpdate')->name('admin.post.update');
    Route::get('admin/report/user-list', 'AdminController@userList')->name('admin.userList');
    Route::get('admin/report/post-list', 'AdminController@postList')->name('admin.postList');
});
